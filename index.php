
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Home</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="fontawesome/css/all.min.css">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
</head>

<body>

<?php include("nav.php");?>

<!--Content-->

<div class="banner">

<div id="carouselExampleSlidesOnly" class="carousel slide " data-ride="carousel">
<div class="carousel-inner">
<div class="carousel-item active">
<img src="images/about_img.jpg" class="img-fluid" alt=".">
</div>
<div class="carousel-item">
<img src="images/about_img1.jpg" class="img-fluid" alt="...">
</div>
<div class="carousel-item">
<img src="images/about_img2.jpg" class="img-fluid" alt="...">
</div>
</div>
</div>

</div>

<!--Article-->
<div class="service-article mt-3 mb-5">
<div class="container">
<h2 class="text-center">We Provide Best Service For Your Business</h2>
<div class="row">
<div class="col-md-3 col-xs-6 px-2">
<div class=" service-box">
<img src="images/production-2.png" class="img-fluid" alt="">
<p>Video production</p>
</div>
</div>
<div class="col-md-3 col-xs-6 px-2">
<div class="service-box">
<img src="images/design.png" class="img-fluid" alt="">
<p>Graphic Design & Printing</p>
</div>
</div>

<div class="col-md-3 col-xs-6 px-2">
<div class="service-box">
<img src="images/production.png" class="img-fluid" alt="">
<p>VFX,3D and Visualization</p>
</div>
</div>
<div class="col-md-3 col-xs-6 px-2">
<div class="service-box">
<img src="images/design.png" class="img-fluid" alt="">
<p>Event Management</p>
</div>
</div>
</div>
</div>
</div>

<div class="about-us-article">
<div class="container">
<div class="row">
<div class="col-md-4">
<img src="images/about_img.png" class="img-fluid" alt="">
</div>

<div class="col-md-8 about-us-media">
<h5 class="about-title">About us</h5>
<h2>Welcome To<br>Myanmar Media Linkage</h2>
<p>Myanmar Media Linkage is initially established as a partnership business by local media and IT
professionals in 2012. It was reformed into a company limited in 2014 to expand business and to be more
competitive in the market with our innovative and creative services. We mainly expertise in two sectors,
media production and IT solutions. </p>
<p>We are a leading web development company with our in-house local designers and developers that specialize
in providing IT and web solutions. From web design, web development, eCommerce platforms to any website and
applications, you name it, we’ve done it. </p>
<p>
<a href="" class="readmore">Read More <i class="fas fa-angle-double-right"></i></a>
</p>
</div>
</div>
</div>
</div>


<div class="container-fluid p-0">
<h2 class="d-flex justify-content-center" style="color: black;">Visual Works</h2>


<ul class="visual-works">
<li  data-filter ="all">All</li>
<li  data-filter =".news-products">News Product</li>
<li  data-filter =".product-release">Product Release</li>
<li  data-filter =".up-coming">Up Coming</li>
</ul>




<div  class="filter-container row p-0 m-0 wow fadeInDown">

<div class="col-sm-6 col-lg-3 p-0 mix product-release ">
<img src="gallery/img-1.jpg" alt="" class="img-fluid">
</div>


<div class="col-sm-6 col-lg-3 p-0 mix news-products up-coming">
<img src="gallery/img-2.jpg" alt="" class="img-fluid">
</div>


<div class="col-sm-6 col-lg-3 p-0 mix product-release ">
<img src="gallery/img-3.jpg" alt="" class="img-fluid">
</div>

<div class="col-sm-6 col-lg-3 p-0 mix news-products">
<img src="gallery/img-4.jpg" alt="" class="img-fluid">
</div>

<div class="col-sm-6 col-lg-3 p-0 mix product-release up-coming">
<img src="gallery/img-5.jpg" alt="" class="img-fluid">
</div>

<div class="col-sm-6 col-lg-3 p-0 mix news-products product-release">
<img src="gallery/img-4.jpg" alt="" class="img-fluid" >
</div>
<div class="col-sm-6 col-lg-3 p-0 mix news-products">
<img src="gallery/img-3.jpg" alt="" class="img-fluid" >
</div>
<div class="col-sm-6 col-lg-3 p-0 mix news-products up-coming">
<img src="gallery/img-1.jpg" alt="" class="img-fluid">
</div>
</div>
</div>
</div>
</div>


<div class="container-fluid projects-section">

<div class="row py-5 projects">
<div class="col-md-8 text-center">
Want to see more? Click on the button at the right side!
</div>

<br>

<div class="col-md-4" style="margin-top:14px;">

<a href="" class="full-projects">Full projects <i class="fas fa-angle-double-right"></i></a>

</div>
</div>

</div>

</div>
</div>



<!--swiper-->
<div class="swiper-container">

<div class="swiper-wrapper">

<div class="swiper-slide">
<img src="images/logo1.jpg" class="img-fluid" alt="">
</div>

<div class="swiper-slide">
<img src="images/logo2.jpg" class="img-fluid" alt="">
</div>

<div class="swiper-slide">
<img src="images/logo3.jpg" class="img-fluid" alt="">
</div>

<div class="swiper-slide">
<img src="images/logo4.jpg" class="img-fluid" alt="">
</div>

<div class="swiper-slide">
<img src="images/logo5.jpg" class="img-fluid" alt="">
</div>

<div class="swiper-slide">
<img src="images/logo6.jpg" class="img-fluid" alt="">
</div>

<div class="swiper-slide">
<img src="images/logo7.jpg" class="img-fluid" alt="">
</div>

<div class="swiper-slide">
<img src="images/logo8.jpg" class="img-fluid" alt="">
</div>

<div class="swiper-slide">
<img src="images/logo9.jpg" class="img-fluid" alt="">
</div>

</div>

</div>

<div class="swiper-pagination">

</div>


<!--Footer-->
<footer class="footer-area">
<div class="container-fluid">
<div class="row text-center">
<div class="col-md-4 sm-12 p-4 bg-ingfo">
<i class="far fa-envelope"></i>
Email <br>
info@mml.com.mm
</div>

<div class="col-md-4 sm-12 p-4 bg-time">
<i class="far fa-clock"></i>
Working hours <br>
Mon-Fri 9AM - 6PM
</div>

<div class="col-md-4  sm-12 p-4 bg-contact">
<i class="fas fa-phone-alt"></i>
Telephone <br>
+959 9759 09701 <br>
+959 4411 95962
</div>
</div>
</div>
<!--map-->
<div class="container-fluid map">
<p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3820.0439034945616!2d96.13867961481736!3d16.774491188448625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c1eb0c6cb9cc5b%3A0x8d8d6b33d88a2880!2sMyanmar%20Media%20Linkage!5e0!3m2!1sen!2smm!4v1599454493335!5m2!1sen!2smm" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></p>
  
  </div>
  
  
  
  <section class="info">
  <div class="container my-3">
  <div class="row">
  <div class="col-md-4">
  <h6>HEAD OFFICE</h6>
  <p>MAPCO Building.No(100).3rd Floor,Wardan Street& Kan Nar
  Street Beside the concrete Express way Wardan Port Area
  Lanmadaw Township Yangon
  </p><br>
  
  
  <h6>OFFICE ADDRESS</h6>
  <p>MAPCO Building.No(100).3rd Floor,Wardan Street& Kan Nar
  Street Beside the concrete Express way Wardan Port Area
  Lanmadaw Township Yangon</p>
  
  </div>
  
  <div class="col-md-4 ">
  <h6>USEFUL LINKS</h6>
  
  <div class="row">
  <div class="col-md-6">
  <ul class="list-unstyled">
  <li>Home</li>
  <li>Services</li>
  <li>News</li>
  <li>Career</li>
  <li>About us</li>
  </ul>
  
  </div>
  <div class="col-md-6">
  <ul class="list-unstyled">
  <li>Carrer</li>
  <li>Reviews</li>
  <li>Terms & Conditions</li>
  <li>Help</li>
  <li>Events</li>
  </ul>
  </div>
  </div>
  
  
  </div>
  
  
  <div class="col-md-4">
  <h6>Events</h6>
  
  <div class="container my-3">
  <div class="row">
  <div class="col-md-6">
  <img src="images/mml1.jpg" class="img-fluid"alt="">
  </div>
  <div class="col-md-6">
  <img src="images/mml2.jpg" class="img-fluid"alt="">
  </div>
  </div>
  
  <div class="row">
  <div class="col-md-6">
  <img src="images/mml3.jpg" class="img-fluid"alt="">
  </div>
  <div class="col-md-6">
  <img src="images/mml4.jpg" class="img-fluid"alt="">
  </div>
  </div>
  </div>
  
  </div>
  </div>
  </div>
  </section>
  
  <div class="copyright-wrap py-3">
  <div class="container">
  <div class="row">
  <div class="col-12">
  <div class="copyright-text text-center">
  
  <p>© Copyright 2019 Myanmar Media Linkage. All rights reserved.</p>
  
  </div>
  </div>
  </div>
  </div>
  </div>
  
  </div>
  
  
  </footer>
  
  
  
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
  integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
  crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
  integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
  crossorigin="anonymous"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.filterizr.min.js"></script>
  <script src="js/jquery-min.js"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  
  
  <script src="js/mixitup.min.js"></script>
  
  
  
  <script>
  
  var swiper = new Swiper('.swiper-container',{
    slidesPerView: 1,
    spaceBetween: 5,
    
    breakpoints:{
      640:{
        slidesPerView: 2,
        spaceBetween: 5,
      },
      768: {
        slidesPerView: 4,
        spaceBetween: 10,
      },
      1024:{
        slidesPerView: 5,
        spaceBetween: 15,
      },
    }
    
  });
  </script>
  
  <script>
  var mixer = mixitup('.filter-container');
  var mixer = mixitup( filter-containerEl);
  var mixer = mixitup( filter-containerEl, {
    selectors: {
      target: '.blog-item'
    },
    animation: {
      duration: 300
    }
  });
  </script>
  
  
  </body>
  
  </html>